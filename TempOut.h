 /** @file TempOut.h
  * determines the temperature at the location of the sensor and adjusts output to the heating pad
  * accordingly
 *    
 * 
 *  @author Jacob Everest
 *  @date  2020-Nov-11 Original file
 */
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif

Adafruit_MPU6050 mpu;

void tempout(void* params_2);
