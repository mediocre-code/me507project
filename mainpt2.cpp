// Basic demo for accelerometer readings from Adafruit MPU6050

#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif

Adafruit_MPU6050 mpu;
int count = 0;

void tempout(void* params_2)
{
  pinMode(A0, OUTPUT);
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("MPU6050 Found!");

  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  Serial.print("Accelerometer range set to: ");
  switch (mpu.getAccelerometerRange()) {
  case MPU6050_RANGE_2_G:
    Serial.println("+-2G");
    break;
  case MPU6050_RANGE_4_G:
    Serial.println("+-4G");
    break;
  case MPU6050_RANGE_8_G:
    Serial.println("+-8G");
    break;
  case MPU6050_RANGE_16_G:
    Serial.println("+-16G");
    break;
  }
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  Serial.print("Gyro range set to: ");
  switch (mpu.getGyroRange()) {
  case MPU6050_RANGE_250_DEG:
    Serial.println("+- 250 deg/s");
    break;
  case MPU6050_RANGE_500_DEG:
    Serial.println("+- 500 deg/s");
    break;
  case MPU6050_RANGE_1000_DEG:
    Serial.println("+- 1000 deg/s");
    break;
  case MPU6050_RANGE_2000_DEG:
    Serial.println("+- 2000 deg/s");
    break;
  }

  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  Serial.print("Filter bandwidth set to: ");
  switch (mpu.getFilterBandwidth()) {
  case MPU6050_BAND_260_HZ:
    Serial.println("260 Hz");
    break;
  case MPU6050_BAND_184_HZ:
    Serial.println("184 Hz");
    break;
  case MPU6050_BAND_94_HZ:
    Serial.println("94 Hz");
    break;
  case MPU6050_BAND_44_HZ:
    Serial.println("44 Hz");
    break;
  case MPU6050_BAND_21_HZ:
    Serial.println("21 Hz");
    break;
  case MPU6050_BAND_10_HZ:
    Serial.println("10 Hz");
    break;
  case MPU6050_BAND_5_HZ:
    Serial.println("5 Hz");
    break;
  }
  for (;;)
  {
    /* Get new sensor events with the readings */
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

  Serial.print("Temperature: ");
  Serial.print(temp.temperature);
  Serial.println(" degC");
  if (temp.temperature < 26.00 )
  {
    count = 0;
  }
  else
  {
    count = 1;
  }
  Serial.println(count);

  if (count == 0)
  {
    digitalWrite(A0, HIGH);
  }
  else
  {
    digitalWrite(A0, LOW);
  }
  
  

  Serial.println("");
  delay(1000);
  }
}

/** @brief   Task which simulates a motor.
 *  @details This task runs at precise interfals using @c vTaskDelayUntil() and
 *           sort of simulates a motor whose duty cycle is controlled by a
 *           power level sent from the UI task. The simulation is just a very
 *           simple implementation of a first-order filter. 
 *  @param   p_params A pointer to function parameters which we don't use.
 */
// void task_sim (void* params)
// {
//     (void)params;                             // Shuts up a compiler warning

//     // Set up the variables of the simulation here
    
//     float sim_A = 0.5;

//     float sim_B = 1.0 - sim_A;

//     float sim_speed = 0.0;

//     const TickType_t sim_period = 50;         // RTOS ticks (ms) between runs

//     int EN_pin = PA10;
//     digitalWrite(EN_pin,HIGH);

//     int PWM_low_pin= PB5;
//     pinMode(PWM_low_pin,OUTPUT);
//     digitalWrite(PWM_low_pin,LOW);

//     int PWM_signal_pin=PB4;
//     pinMode(PWM_signal_pin,OUTPUT);
//     // Initialise the xLastWakeTime variable with the current time.
//     // It will be used to run the task at precise intervals
//     TickType_t xLastWakeTime = xTaskGetTickCount();
//     for (;;)
//     {
//       if (count == 1) 
//         {
//           // The simulation code goes here...you probably knew that already
//           sim_speed = 150;

//           analogWrite (PWM_signal_pin, (uint32_t) sim_speed);

//           // This type of delay waits until it has been the given number of RTOS
//           // ticks since the task previously began running. This prevents timing
//           // inaccuracy due to not accounting for how long the task took to run
//           vTaskDelayUntil (&xLastWakeTime, sim_period);
//         }
//       else
//         {
//           sim_speed = 0;

//           analogWrite (PWM_signal_pin, (uint32_t) sim_speed);

//           // This type of delay waits until it has been the given number of RTOS
//           // ticks since the task previously began running. This prevents timing
//           // inaccuracy due to not accounting for how long the task took to run
//           vTaskDelayUntil (&xLastWakeTime, sim_period);
          
//         }
//     }
// }

void setup() 
{
  Serial.begin(115200);
  while (!Serial)
    delay(10); // will pause Zero, Leonardo, etc until serial console opens

  Serial.println("Adafruit MPU6050 test!");

   xTaskCreate (tempout,
                 "Simul.",
                 1024,                            // Stack size
                 NULL,
                 5,                               // Priority
                 NULL);
  //  xTaskCreate (task_sim,
  //                "Sim.",
  //                1024,                            // Stack size
  //                NULL,
  //                3,                               // Priority
  //                NULL);
  #if (defined STM32L4xx || defined STM32F4xx)
        vTaskStartScheduler (); //schedule said task
  #endif

  Serial.println("");
  delay(100);
}

void loop() 
{

}