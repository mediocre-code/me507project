/** @file main.cpp
 *    This file contains a partially written program which students should
 *    greatly improve so that it runs a very crude simulation of a motor. 
 *
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 * 
 *  @date    14 Nov 2020 Original file
 */



/** @brief   Read an integer from a serial device, echoing input and blocking.
 *  @details This function reads an integer which is typed by a user into a
 *           serial device. It uses the Arduino function @c readBytes(), which
 *           blocks the task which calls this function until a character is
 *           read. When any character is received, it is echoed through the
 *           serial port so the user can see what was typed. Only decimal
 *           integers are supported; negative integers beginning with a single
 *           @c - sign or positive ones with a @c + will work. 
 * 
 *           @b NOTE: The serial device must have its timeout set to a very
 *           long time, or this function will malfunction. A recommended call:
 *           @code
 *           Serial.setTimeout (0xFFFFFFFF);
 *           @endcode
 *           Assuming that the serial port named @c Serial is being used.
 *  @param   stream The serial device such as @c Serial used to communicate
 */

#include <Arduino.h>
#include <Wire.h>
#include <BH1750.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include <LightClass.h>

int lightflag = 0;
int lightflagcompare = 0;
uint32_t count = 0;

/** @brief   Task which reads light level. 
 *  @details This task reads the light level and saves it through a class, then passes the value to the
 *           next task which will in turn actuate the motor or stay the same based on the value. It has
 *           a threshold value that it compares to. When the actual light value is above that, lightflag
 *           is 0, and when it's above lightflag is 1. At the end of the next task, lightflag's value is 
 *           saved into another variable (lightflagcompare) so that after lightflag's value in this task
 *           is set, the two can be compared. If they are the same, the motor will be set to run for 0
 *           cycles. If the current lightflag is different from the last lightflag the motor will be set
 *           to run for 60 cycles.
 *  @param   p_params A pointer to function parameters which we don't use.
 */

void use_LightClass(void* p_params)
{
  (void) p_params;
  BH1750 lightMeter(0x23);
  if (lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {
    Serial.println(F("BH1750 Advanced begin"));
  }
  else {
    Serial.println(F("Error initialising BH1750"));
  }
  for(;;)
  {
    LightClass Lighting (lightMeter.readLightLevel());
    Serial << "Light Level is: " <<  Lighting.lightlevel() << endl;
    if (Lighting.lightlevel() > 500)
    {
      lightflag = 1;
      Serial << "It is Daytime" << endl;
    }
    else
    {
      lightflag = 0;
      Serial << "It is night time" << endl;  
    }
    if (lightflag != lightflagcompare)
    {
      count = 60;
    }
    delay (1000);
  }
}

/** @brief   Task which simulates a motor.
 *  @details This task runs at precise interfals using @c vTaskDelayUntil() and
 *           sort of simulates a motor whose duty cycle is controlled by a
 *           power level sent from the UI task. It is the same setup as our simulation from
 *           class but it runs an actual motor.
 *  @param   p_params A pointer to function parameters which we don't use.
 */
void task_sim (void* params)
{
    (void)params;                             // Shuts up a compiler warning

    // Set up the variables of the simulation here

    int32_t duty_cycle_var;
    
    float sim_A = 0.5;

    float sim_B = 1.0 - sim_A;

    float sim_speed = 0.0;

    const TickType_t sim_period = 50;         // RTOS ticks (ms) between runs

    int EN_pin = PA10;
    digitalWrite(EN_pin,HIGH);

    int PWM_low_pin= PB5;
    pinMode(PWM_low_pin,OUTPUT);
    digitalWrite(PWM_low_pin,LOW);

    int PWM_signal_pin=PB4;
    pinMode(PWM_signal_pin,OUTPUT);
    // Initialise the xLastWakeTime variable with the current time.
    // It will be used to run the task at precise intervals
    TickType_t xLastWakeTime = xTaskGetTickCount();
    for (;;)
    {
      if (count != 0) 
      //if (lightflag == 0)
        {
          // The simulation code goes here...you probably knew that already
          duty_cycle_var = 150;
          //duty_cycle.get (duty_cycle_var);
          sim_speed = sim_speed * sim_A + duty_cycle_var * sim_B;

         
          analogWrite (PWM_signal_pin, (uint32_t) sim_speed);
          count = count - 1;

          // This type of delay waits until it has been the given number of RTOS
          // ticks since the task previously began running. This prevents timing
          // inaccuracy due to not accounting for how long the task took to run
          vTaskDelayUntil (&xLastWakeTime, sim_period);
        }
      else
        {
          // if (lightflag != lightflagcompare)
          // {
          //   count = 200;
          // }
          // The simulation code goes here...you probably knew that already
          duty_cycle_var = 0;
          //duty_cycle.get (duty_cycle_var);
          sim_speed = sim_speed * sim_A + duty_cycle_var * sim_B;

          analogWrite (PWM_signal_pin, (uint32_t) sim_speed);

          // This type of delay waits until it has been the given number of RTOS
          // ticks since the task previously began running. This prevents timing
          // inaccuracy due to not accounting for how long the task took to run
          vTaskDelayUntil (&xLastWakeTime, sim_period);
          
        }
      lightflagcompare = lightflag;
        
    }
}

/** @brief   Arduino setup function which runs once at program startup.
 *  @details This function sets up a serial port for communication and creates
 *           the tasks which will be run.
 */

void setup() 
{
  Serial.begin(115200);
  Wire.begin();
  // put your setup code here, to run once:
  xTaskCreate (use_LightClass, "LightingPlease", 1256, NULL, 3, NULL); //make the task
  xTaskCreate (task_sim,
                 "Simul.",
                 1024,                            // Stack size
                 NULL,
                 5,                               // Priority
                 NULL);
  #if (defined STM32L4xx || defined STM32F4xx)
        vTaskStartScheduler (); //schedule said task
  #endif
}

/** @brief   Nothing to see here.
 *  @details I SAID NOTHING TO SEE HERE.
 */

void loop() 
{
  //Abandon all hope ye who enter here
}