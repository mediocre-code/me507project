/** @brief   Task which measures the lightvalue 
 *  @details This task measures lightvalue and decides if it has changed 
 * enough since it last read to warrant running the motor
 *  @param   p_params A pointer to function parameters which we don't use.
 */
#include "useLightclass.h"

Share <uint32_t> motortime ("motor run");

void use_LightClass(void* p_params)
{
  (void) p_params;
  int lightflag = 0;
  int lightflagcompare = 0;
  BH1750 lightMeter(0x23);
  if (lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {
    Serial.println(F("BH1750 Advanced begin"));
  }
  else {
    Serial.println(F("Error initialising BH1750"));
  }
  for(;;)
  {
    LightClass Lighting (lightMeter.readLightLevel());
    Serial << "Light Level is: " <<  Lighting.lightlevel() << endl;
    if (Lighting.lightlevel() > 500)
    {
      lightflag = 1;
      Serial << "It is Daytime" << endl;
    }
    else
    {
      lightflag = 0;
      Serial << "It is night time" << endl;  
    }
    if (lightflag != lightflagcompare)
    {
      count = 60;
    }
    delay (1000);
    lightflagcompare = lightflag;
    motortime.put(count)
  }
}