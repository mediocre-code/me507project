/** @brief   Task which measures the lightvalue 
 *  @details This task measures lightvalue and decides if it has changed 
 * enough since it last read to warrant running the motor
 *  @param   p_params A pointer to function parameters which we don't use.
 */
#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"
#include <Wire.h>
#include <BH1750.h>

void use_LightClass(void* p_params);