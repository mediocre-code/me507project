
#include "MotorRun.h"

/** @brief   Task which simulates a motor.
 *  @details This task runs at precise interfals using @c vTaskDelayUntil() and
 *           sort of simulates a motor whose duty cycle is controlled by a
 *           power level sent from the UI task. The simulation is just a very
 *           simple implementation of a first-order filter. 
 *  @param   p_params A pointer to function parameters which we don't use.
 */

extern Share <uint32_t> count;

void task_sim (void* params)
{
    (void)params;                             // Shuts up a compiler warning

    // Set up the variables of the simulation here

    int32_t duty_cycle_var;
    
    float sim_A = 0.5;

    float sim_B = 1.0 - sim_A;

    float sim_speed = 0.0;

    const TickType_t sim_period = 50;         // RTOS ticks (ms) between runs

    int EN_pin = PA10;
    digitalWrite(EN_pin,HIGH);

    int PWM_low_pin= PB5;
    pinMode(PWM_low_pin,OUTPUT);
    digitalWrite(PWM_low_pin,LOW);

    int PWM_signal_pin=PB4;
    pinMode(PWM_signal_pin,OUTPUT);
    // Initialise the xLastWakeTime variable with the current time.
    // It will be used to run the task at precise intervals
    TickType_t xLastWakeTime = xTaskGetTickCount();
    for (;;)
    {
      motortime.get(count);
      if (count != 0) 
      //if (lightflag == 0)
        {
          
          // The simulation code goes here...you probably knew that already
          duty_cycle_var = 150;
          //duty_cycle.get (duty_cycle_var);
          sim_speed = sim_speed * sim_A + duty_cycle_var * sim_B;

         
          analogWrite (PWM_signal_pin, (uint32_t) sim_speed);
          count = count - 1;

          // This type of delay waits until it has been the given number of RTOS
          // ticks since the task previously began running. This prevents timing
          // inaccuracy due to not accounting for how long the task took to run
          vTaskDelayUntil (&xLastWakeTime, sim_period);
        }
      else
        {
          // if (lightflag != lightflagcompare)
          // {
          //   count = 200;
          // }
          // The simulation code goes here...you probably knew that already
          duty_cycle_var = 0;
          //duty_cycle.get (duty_cycle_var);
          sim_speed = sim_speed * sim_A + duty_cycle_var * sim_B;

          analogWrite (PWM_signal_pin, (uint32_t) sim_speed);

          // This type of delay waits until it has been the given number of RTOS
          // ticks since the task previously began running. This prevents timing
          // inaccuracy due to not accounting for how long the task took to run
          vTaskDelayUntil (&xLastWakeTime, sim_period);
          
        }
    }
}