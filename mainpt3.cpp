/** @file mainpt3.cpp
 *    This file contains a moisture sensor code. 
 *
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 * 
 *  @date    28 Nov 2020 Original file
 */



/** @brief   Read moisture level and respond accordingly.
 *  @details Read moisture value and compare to a predetermined value that
 *           we want to keep the moisture level above
 */

#include "Adafruit_seesaw.h"
#include <Arduino.h>
#include <Wire.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
Adafruit_seesaw ss;

/** @brief   Light sensor and response.
 *  @details light sensor with a boolean. If moisture level is too low it
 *           will turn on an LED to alert the user to water it
 *  @param   p_params2
 */

void moisturesense(void* p_param2)
{
  float tempC = ss.getTemp();
  uint16_t capread = ss.touchRead(0); 
  Serial.println("seesaw Soil Sensor example!");
  
  if (!ss.begin(0x36)) {
    Serial.println("ERROR! seesaw not found");
    while(1);
  } else {
    Serial.print("seesaw started! version: ");
    Serial.println(ss.getVersion(), HEX);
  }
  for (;;)
  {
    if (capread < 40)
    {
      digitalWrite (A1,HIGH);
    }
    else
    {
      digitalWrite (A1, LOW);
    }
    delay (1000);
  }
}

/** @brief   Arduino setup function which runs once at program startup.
 *  @details This function sets up a serial port for communication and creates
 *           the tasks which will be run.
 */

void setup() {
  Wire.begin();
  Serial.begin(115200);
  // put your setup code here, to run once:
  xTaskCreate (moisturesense,
                 "Moisture.",
                 1024,                            // Stack size
                 NULL,
                 5,                               // Priority
                 NULL);
  #if (defined STM32L4xx || defined STM32F4xx)
        vTaskStartScheduler (); //schedule said task
  #endif
}

void loop() 
{
  //Nothing here  
}