/** @brief   Task which keeps us safe.
 *  @details This task keeps track of how long the heater has been on and 
 * lets us know if that's been too long 
 *  @param   parameters A pointer to function parameters which we don't use.
 */
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif

void safety (void* parameters);