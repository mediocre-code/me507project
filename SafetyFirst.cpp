/** @brief   Task which keeps us safe.
 *  @details This task keeps track of how long the heater has been on and 
 * lets us know if that's been too long 
 *  @param   parameters A pointer to function parameters which we don't use.
 */
#include "SafetyFirst.h"
extern Share <uint32_t> Temp_value;
Share <bool> DANGER ("Danger flag");

void safety (void* parameters)
{
    uint32_t timer;
    for (;;)
    {
        bool danger
        Temp_value.get(timer);
        if (timer > 1800)
        {
            danger = true;
        }
        else
        {
            danger = false;
        }
        DANGER.put(danger);
        
    }
}