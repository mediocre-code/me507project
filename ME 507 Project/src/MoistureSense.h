//*****************************************************************************
/** @file    MoistureSense.h
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom 
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-NOV-30 Samara finished the file
 * 
*/
#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"
#include <Wire.h>
#include "Adafruit_seesaw.h"


/**  @brief  A task that operates a soil moisture sensor.
 *  @details This code refers to a task that operates the Adafruit STEMMA 
 *           Soil Sensor. It uses the Wire and Adafruit seesaw classes 
 *           associated with the header files Wire.h and Adafruit_seesaw.h.
 *           This code creates an object with the Adafruit class and calls on
 *           the touchRead() method to read the capacitance of the soil, which
 *           directly translates to soil moisture.
 *  @param   p_params A pointer to function parameters which we don't use.
*/
void Moisture_Sensor_Task (void* p_params);        // The task function