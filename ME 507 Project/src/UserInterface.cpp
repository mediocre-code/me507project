//*****************************************************************************
/** @file    UserInterface.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-NOV-30 Samara finished the file
 *
*/

#include "UserInterface.h"

Share<float> Capacitive_Setpoint ("Desired Soil Moisture");
Share<float> Humidity_Setpoint ("Desired Air Humidity");
Share<float> Brightness_Setpoint ("Desired Light");
Share<float> Temp_Setpoint ("Desired Temperature");


/** @brief   Read an integer from a serial device, echoing input and blocking.
 *  @details This function reads an integer which is typed by a user into a
 *           serial device. It uses the Arduino function @c readBytes(), which
 *           blocks the task which calls this function until a character is
 *           read. When any character is received, it is echoed through the
 *           serial port so the user can see what was typed. Only decimal
 *           integers are supported; negative integers beginning with a single
 *           @c - sign or positive ones with a @c + will work. 
 * 
 *           @b NOTE: The serial device must have its timeout set to a very
 *           long time, or this function will malfunction. A recommended call:
 *           @code
 *           Serial.setTimeout (0xFFFFFFFF);
 *           @endcode
 *           Assuming that the serial port named @c Serial is being used.
 *  @param   stream The serial device such as @c Serial used to communicate
 */
int32_t parseIntWithEcho (Stream& stream)
{
    const uint8_t MAX_INT_DIGITS = 24;        // More than a 64-bit integer has
    char ch_in = 0;                           // One character from the buffer
    char in_buf[MAX_INT_DIGITS];              // Character buffer for input
    uint8_t count = 0;                        // Counts characters received

    // Read until return is received or too many characters have been read.
    // The readBytes function blocks while waiting for characters
    while (true)
    {
        stream.readBytes (&ch_in, 1);         // Read (and wait for) characters
        in_buf[count++] = ch_in;
        stream.print (ch_in);                 // Echo the character
        if (ch_in == '\b' && count)           // If a backspace, back up one
        {                                     // character and try again
            count -= 2;
        }
        if (ch_in == '\n' || count >= (MAX_INT_DIGITS - 1))
        {
            in_buf[count] = '\0';             // String must have a \0 at end
            return atoi (in_buf);
        }
    }
}


/** @brief  Task which handles interfacing with the user
 *  @details This task communicates with the user and sets their input to 
 *           shares used by other tasks. The different variables the 
 *           user can change are the soil moisture, percent humidity,
 *           brightness level, and temperature.
 *  @param   p_params A pointer to function parameters which we don't use.
*/ 
void UI_Setpoint_Task (void* p_params)
{
    (void)p_params;                     // Shuts up a compiler warning

    // Set up the variable used to hold each setpoint
    uint32_t moisture = 0;
    uint32_t humidity = 0;
    uint32_t light = 0;
    uint32_t temperature = 0;

    // Set up the states for the State transition model
    uint8_t state;
    uint8_t S0_INIT = 0;
    uint8_t S1_WAIT = 1;
    uint8_t S2_SEND = 2;

    // Set the timeout time to forever
    Serial.setTimeout (0xFFFFFFFF);

    // Set the initial state to State 0 and start the for(;;) loop
    state = S0_INIT;

    for(;;)
    {
        if(state  == S0_INIT)
        {
            // State 0 Code
            
            // If there is no value for moisture, send a prompt through the Serial port
            if (!moisture)
            {
                Serial << "(1/4) Enter a desired soil moisture value between 200 (very dry) to 2000 (very wet): " << endl;

                // Transition to State 1
                state = S1_WAIT;

            }

            // If there is no value for humidity, send a prompt through the Serial port
            else if (!humidity)
            {
                Serial << "(2/4) Enter a desired air humidity between 1 (very dry) and 100 (very humid): " << endl;

                // Transition to State 1
                state = S1_WAIT;

            }

            // If there is no value for light, send a prompt through the Serial port
            else if (!light)
            {
                Serial << "(3/4) Enter a desired brightness level between 1 (very dark) and 65000 (very bright): " << endl;

                // Transition to State 1
                state = S1_WAIT;
            }

            // If there is no value for temperature, send a prompt through the Serial port
            else if (!temperature)
            {
                Serial << "(4/4) Enter a desired temperature in Fahrenheit between 50 and 100 :" << endl;

                // Transition to State 1
                state = S1_WAIT;

            }
        }

        else if(state == S1_WAIT)
        {
            // State 1 Code

            // If there is no value for moisture, run the input from Serial through parseIntWithEcho
            if (!moisture)
            {
                moisture = parseIntWithEcho(Serial);

                // Check if the value is within the limits provided
                if (moisture >= 200 && moisture <= 2000)
                {   
                    state = S2_SEND;
                }   
                else
                {
                    Serial << "Not within the limits provided for soil moisture. Try again." << endl;
                    moisture = 0;
                    state = S0_INIT;
                }
            }

            // If there is no value for moisture, run the input from Serial through parseIntWithEcho
            else if(!humidity)
            {
                humidity = parseIntWithEcho(Serial);

                // Check if the value is within the limits provided
                if (humidity >= 1 && humidity <= 100)
                {   
                    state = S2_SEND;
                }   
                else
                {
                    Serial << "Not within the limits provided for humidity. Try again." << endl;
                    humidity = 0;
                    state = S0_INIT;
                }
            }

            // If there is no value for moisture, run the input from Serial through parseIntWithEcho
            else if(!light)
            {
                light = parseIntWithEcho(Serial);

                // Check if the value is within the limits provided
                if (light >= 1 && light <= 65000)
                {   
                    state = S2_SEND;
                }   
                else
                {
                    Serial << "Not within the limits provided for brightness. Try again." << endl;
                    light = 0;
                    state = S0_INIT;
                }
            }

            // If there is no value for moisture, run the input from Serial through parseIntWithEcho
            else if(!temperature)
            {
                temperature = parseIntWithEcho(Serial);

                // Check if the value is within the limits provided
                if (temperature >= 50 && temperature <= 100)
                {   
                    state = S2_SEND;
                }   
                else
                {
                    Serial << "Not within the limits provided for temperature. Try again." << endl;
                    temperature = 0;
                    state = S0_INIT;
                }
            }
        }

        else if (state == S2_SEND)
        {
            // State 2 Code

            // If there is a value for moisture, put it into the share
            if (moisture)
            {
                Capacitive_Setpoint.put((float) moisture);
                
            }

            // If there is a value for humidity, put it into the share
            if (humidity)
            {
                Humidity_Setpoint.put((float) humidity);
                
            }

            // If there is a value for light, put it into the share
            if (light)
            {
                Brightness_Setpoint.put((float) light);
                
            }

            // If there is a value for temperature, put it into the share
            if (temperature)
            {
                Temp_Setpoint.put((float) temperature);
                
            }

            // Once each variable has a value, set all the variables to zero so
            // the UI can prompt the user again
            if(moisture && humidity && light && temperature)
            {
                moisture = 0;
                humidity = 0;
                light = 0;
                temperature = 0;
            }

            // Set the state to State 0
            state = S0_INIT;
        }  
    }
}
