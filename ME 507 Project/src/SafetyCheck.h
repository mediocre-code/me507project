//*****************************************************************************
/** @file    SolenoidValve.h
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-NOV-30 Samara finished the file
 *
*/
#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"

/**  @brief   Headers for a base class to operate a Solenoid valve.
 *  @details This code includes a task that operates a simple Solenoid valve by
 *           writing to the pin that the valve is attached to either high or low. HIGH
 *           opens the valve, while LOW closes it. 
 *  @param   p_params A pointer to function parameters which we don't use.
*/
void Safety_Check_Task (void* p_params);