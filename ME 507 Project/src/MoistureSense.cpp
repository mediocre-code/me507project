//*****************************************************************************
/** @file    MoistureSense.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom 
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-NOV-30 Samara finished the file
 * 
*/

#include "MoistureSense.h"

// Create the share for soil moisture
Share<float> Capacitive_Value ("Measured Soil Moisture");

/**  @brief  A task that operates a soil moisture sensor.
 *  @details This code contains a task that operates the Adafruit STEMMA 
 *           Soil Sensor. It uses the Wire and Adafruit seesaw classes 
 *           associated with the header files Wire.h and Adafruit_seesaw.h.
 *           This code creates an object with the Adafruit class and calls on
 *           the touchRead() method to read the capacitance of the soil, which
 *           directly translates to soil moisture.
 *  @param   p_params A pointer to function parameters which we don't use.
*/ 
void Moisture_Sensor_Task (void* p_params)
{
    (void)p_params;                     // Shuts up a compiler warning

    // Create an object from the Adafruit class to communicate with the sensor
    Adafruit_seesaw ss;

    // Begin communication with the sensor
    // If unable connect, print an error to Serial
    if (!ss.begin(0x36)) 
    {
        Serial.println("ERROR! seesaw not found");
        while(1);
    } 
    else 
    {
        Serial.print("seesaw started! version: ");
        Serial.println(ss.getVersion(), HEX);
    }

    // Create variable to hold capacitance
    float capread;

    // Set up states for state transition model
    uint8_t state;
    uint8_t S0_INIT = 0;
    uint8_t S1_READ = 1;
    uint8_t S2_SEND = 2;

    // Set the state to State 0 and begin the for(;;) loop
    state = S0_INIT;

    for(;;)
    {
        if(state == S0_INIT)
        {
            // State 0 Code

            // Read the sensor
            capread = ss.touchRead(0);

            // Transition to State 1
            state = S1_READ;

        }
        else if(state ==S1_READ)
        {
            // State 1 Code

            // If a measurement was made, put it into the share
            // If not, transition to State 0 to read again
            if (capread)
            {
                Capacitive_Value.put(capread);
                state = S0_INIT;
            }
            else
            {
                state = S0_INIT;
            }
        }
    
    }

}