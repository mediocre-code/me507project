/** @file LightClass.h
 *    
 * 
 *  @author Jacob Everest
 *  @date  2020-Nov-11 Original file
 */


/** @brief   class which monitors light affecting the sensor
 *  @details monitors light sensor outputs
 *  @param   p_params A pointer to function parameters which we don't use.
 */
class LightClass
{
private:
    float lightvalue;
public:
    LightClass (float y);
    float lightlevel (void); 
};