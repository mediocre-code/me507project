//*****************************************************************************
/** @file    UserInterface.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-NOV-30 Samara finished the file
 *
*/

#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"

/** @brief  Task which handles interfacing with the user
 *  @details This task communicates with the user and sets their input to 
 *           shares used by other tasks. The different variables the 
 *           user can change are the soil moisture, percent humidity,
 *           brightness level, and temperature.
 *  @param   p_params A pointer to function parameters which we don't use.
*/

void UI_Setpoint_Task (void* p_params);