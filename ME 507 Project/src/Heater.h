//*****************************************************************************
/** @file    Heater.h
 *  @brief   Header file for source code to control a simple heating pad.
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-NOV-20 Samara created the file
 *  @date 2020-DEC-01 Samara finished the file
 *
*/

#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"

/** @brief   Task to control a simple heating pad.
 *  @details This is a task that operates a simple heating by
 *           writing to the pin that the pad is attached to either high or 
 *           low. HIGH turns the pad on, while LOW turns it off. This code 
 *           also tests which state the heating pad should be in by comparing
 *           the measured and desired value for temperature.
*/
void Power_Dissipate_Task (void* p_params);