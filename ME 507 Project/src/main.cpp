//*****************************************************************************
/** @file    MoistureSense.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom 
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-DEC-01 Samara finished the file
 * 
*/

#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"
#include "HumidTempSense.h"
#include "Mister.h"
#include "SolenoidValve.h"
#include "UserInterface.h"
#include "MoistureSense.h"
#include "Heater.h"
#include "SafetyFirst.h"
#include "MotorRun.h"
#include "useLightclass.h"


/** @brief   Arduino setup function which runs once at program startup.
 *  @details This function sets up a serial port for communication and creates 
 *           the tasks which will be run.
 */
void setup() 
{
  Serial.begin (115200);
  Wire.begin();
  delay (2000);
  Serial << "Start FacePlant" << endl;
  xTaskCreate (Temp_Humidity_Sensor_Task,
              "Read Temperature and Humidity",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              4,                               // Priority
              NULL); 
  xTaskCreate (Mist_Actuator_Task,
              "Run the Mist Actuator",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              5,                               // Priority
              NULL); 
  xTaskCreate (Valve_Actuator_Task,
              "Operate the Solenoid Valve",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              6,                               // Priority
              NULL); 
  xTaskCreate (UI_Setpoint_Task,
              "User Interface",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              2,                               // Priority
              NULL); 
  xTaskCreate (S_Motor_Driver_Task,
              "Control the Light Door",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              7,                               // Priority
              NULL); 
  xTaskCreate (Power_Dissipate_Task,
              "Control the Heating Pad",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              4,                               // Priority
              NULL);
  xTaskCreate (use_LightClass,
              "Measure Light",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              5,                               // Priority
              NULL);
  xTaskCreate (safety,
              "Determine if there's danger",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              1,                               // Priority
              NULL);
  // This task referred to a soil moisture sensor that is inoperable
  // xTaskCreate (Moisture_Sensor_Task,
  //             "Measure the Moisture in the Soil",                      // Name for printouts
  //             2048,                            // Stack size
  //             NULL,                            // Parameter(s) for task fn.
  //             1,                               // Priority
  //             NULL); 

  #if (defined STM32L4xx || defined STM32F4xx)
        vTaskStartScheduler ();
    #endif    
  

}

void loop() {
  // put your main code here, to run repeatedly:
}