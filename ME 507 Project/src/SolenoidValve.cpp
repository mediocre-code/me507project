//*****************************************************************************
/** @file    SolenoidValve.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-NOV-30 Samara finished the file
 *
*/

#include "SolenoidValve.h"

Share<float> Percent_Valve_Open ("Valve Open or Closed");
extern Share<float> Capacitive_Setpoint;
extern Share<float> Capacitive_Value;


/** @brief   Task to control a simple Solenoid valve.
 *  @details This code includes a task that operates a simple Solenoid valve by
 *  writing to the pin that the valve is attached to either high or low. HIGH
 *  opens the valve, while LOW closes it.
 *  @param   p_params A pointer to function parameters which we don't use.
*/
void Valve_Actuator_Task (void* p_params)
{
    // Create the states for this task
    uint8_t state; 
    uint8_t S0_INIT = 0;
    uint8_t S1_CLOSED = 1;
    uint8_t S2_OPEN = 2;
    uint16_t counter;


    // Set pin PB10 (or D6 on Arduino) for OUTPUT
    pinMode(PB10, OUTPUT);

    // Create the variables that will hold the shares
    float capac;
    float desired_moisture_saturation;


    // Set the first state and start the task loop
    state = S0_INIT;
    for(;;)
    {
        // The following code was written for when the Soil Moisture Sensor worked. 
        // When a working Soil Moisture Sensor is found, this can be added back in.

        // if(state == S0_INIT)
        // {
        //     Capacitive_Value.get(capac);
        //     Capacitive_Setpoint.get(desired_moisture_saturation);
            
        
        //     if(capac < desired_moisture_saturation)
        //     {
        //         digitalWrite(PB10, HIGH);
        //         state = S2_OPEN;

        //     }
        //     else if(capac >= desired_moisture_saturation)
        //     {
        //         digitalWrite(PB10, LOW);
        //         state = S1_CLOSED;
        //     }
        // }
        // else if(state == S1_CLOSED)
        // {

            
        // }
        // else if(state == S2_OPEN)
        // {
        //     digitalWrite(PB10, HIGH);
        //     vTaskDelay(3000);
        //     digitalWrite(PB10, LOW);
        //     vTaskDelay(10000);
        //     state = S0_INIT;
        // }

        // Initialize the Solenoid valve in a closed position
        if(state == S0_INIT)
        {
            // Run State 0 code
            digitalWrite(PB10, LOW);
            counter = 0;
            state = S1_CLOSED;
        }
        else if(state == S1_CLOSED)
        {
            // Tells the Safety Check task that the valve is closed
            Percent_Valve_Open.put(0);
            
            // This while loop makes sure that the valve is opened once a day
            while (counter != 23) 
            {
                vTaskDelay((1000*3600) / portTICK_RATE_MS);
                counter++;
            }

            state = S2_OPEN;

        }
        else if(state == S2_OPEN)
        {
            //Tells the Safety Check task that the valve is open
            Percent_Valve_Open.put(1);
            // Opens the valve for 3 seconds and closes it again
            digitalWrite(PB10, HIGH);
            vTaskDelay(3000);
            digitalWrite(PB10, LOW);
            state = S0_INIT;

        }


        


        


    }


}