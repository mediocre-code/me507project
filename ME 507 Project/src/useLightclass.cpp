#include "useLightclass.h"

Share <uint32_t> motortime ("motor run");

void use_LightClass(void* p_params)
{
  (void) p_params;
  int lightflag = 0;
  int lightflagcompare = 0;
  uint16_t count;
  BH1750 lightMeter(0x23);

  if (lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {
    Serial.println(F("BH1750 Advanced begin"));
  }
  else {
    Serial.println(F("Error initialising BH1750"));
  }

  LightClass Lighting (lightMeter.readLightLevel());

  for(;;)
  {
    
    
    if (Lighting.lightlevel() > 500)
    {
      lightflag = 1;
      
    }
    else
    {
      lightflag = 0;
       
    }
    if (lightflag != lightflagcompare)
    {
      count = 60;
    }
    delay (1000);
    lightflagcompare = lightflag;
    motortime.put(count);
  }
}