//*****************************************************************************
/** @file    Mister.h
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-DEC-01 Samara finished the file
 *
*/

#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"
#include "Servo.h"

/**  @brief  A task that operates a servo motor used to spray a mister.
 *           
 *  @details This file contains code for a task that operates a servo 
 *           motor through the use of the Servo class provided in the file 
 *           associated with the header Servo.h. This task tests compares the 
 *           desired humidity and the measured humidity to determine whether 
 *           to run the servo. If it finds that the humidity is too low, it 
 *           will activate the motor; if not, it will wait for 10 seconds 
 *           before checking again.
 *  @param   p_params A pointer to function parameters which we don't use.
*/
void Mist_Actuator_Task (void* p_params);