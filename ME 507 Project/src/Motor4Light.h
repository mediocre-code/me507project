//*****************************************************************************
/** @file    baseshare.h
 *  @brief   Headers for a base class for type-safe, thread-safe task data 
 *           exchange classes.
 *  @details This file contains a base class for classes which exchange data 
 *           between tasks. Inter-task data must be exchanged in a thread-safe 
 *           manner, so the classes which share the data use mutexes or mutual 
 *           exclusion mechanisms to prevent corruption of data. A linked list
 *           of all inter-task data items is kept by this system, and this base
 *           class contains members that handle that linked list. 
 * 
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2014-Oct-18 JRR Created file
 *  @date 2020-Oct-19 JRR Modified for use with Arduino/FreeRTOS platform
 *
*/
#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"
#include "Servo.h"

void S_Motor_Driver_Task (void* p_params);