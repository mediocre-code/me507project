//*****************************************************************************
/** @file    Mister.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-DEC-01 Samara finished the file
 *
*/
#include "Mister.h"

Share<float> Mister_Voltage ("Voltage of the Mister Servo Motor");
extern Share<float> Rel_Humidity;
extern Share<float> Humidity_Setpoint;

/**  @brief   A task that operates a servo motor used to spray a mister.
 *  @details This file contains a task that operates a servo motor through
 *           the use of the Servo class provided in the file associated with
 *           the header Servo.h. This task tests compares the desired humidity
 *           and the measured humidity to determine whether to run the servo. 
 *           If it finds that the humidity is too low, it will activate the 
 *           motor; if not, it will wait for 10 seconds before checking again.
 *  @param   p_params A pointer to function parameters which we don't use.
*/
void Mist_Actuator_Task (void* p_params)
{

    Servo misterservo;                      // Create a Servo object

    misterservo.attach(PA0);                // Attach the misterservo to pin PA0

    // Create the variables to hold measured and desired humidity
    float humidair;
    float desiredhumidair;

    // Set up states for state transition model

    uint8_t state;
    uint8_t S0_INIT = 0;
    uint8_t S1_TEST = 1;
    uint8_t S2_ACTIVATE = 2;
    uint8_t S3_DO_NOTHING = 3;

    // Set the first state to initialize
    state = S0_INIT;

    for(;;)
    {
        if(state == S0_INIT)
        {
            // State 0 Code

            // Pull data from other tasks with shares
            Rel_Humidity.get(humidair);
            Humidity_Setpoint.get(desiredhumidair);
            
            // Change to State 1
            state = S1_TEST;

        }

        else if(state == S1_TEST)
        {
            // State 1 Code

            if(humidair < desiredhumidair)
            {
                // Change to State 2
                state = S2_ACTIVATE;        
            }
            else
            {
                // Change to State 3
                state = S3_DO_NOTHING;
            }
        }

        else if(state == S2_ACTIVATE)
        {
            // State 2 Code

            // Make the servo turn 160 degrees
            misterservo.write(160);

            // Delay half a second
            delay(500);

            // Turn servo back to initial position
            misterservo.write(0);

            // Change to State 0
            state = S0_INIT;

        }

        else if(state == S3_DO_NOTHING)
        {
            // State 3 Code

            // Wait 10 seconds
            delay(10000);

            // Change to State 0
            state = S0_INIT;
        }
        
        
        


    }



}