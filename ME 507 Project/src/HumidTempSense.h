//*****************************************************************************
/** @file    HumidTempSense.h
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-DEC-01 Samara finished the file
 *
*/
#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"
#include <Wire.h>
#include <SparkFun_SHTC3.h>


/**  @brief   A header file for a task used to operate the Qwiic Humidity 
 *           Sensor - SHTC3 to measure temperature and humidity.
 *  @details This task uses the class SHTC3 provided by SparkFun under the 
 *           header SparkFun_SHTC3.h to operate the Qwiic Humidity Sensor. The
 *           code in this task creates an object from the class and calls on
 *           update method to measure the temperature and humidity of an area.
 *           It then sends the data to another task through the use of a share.
 *  @param   p_params A pointer to function parameters which we don't use.
*/
void Temp_Humidity_Sensor_Task (void* p_params);        // The task function