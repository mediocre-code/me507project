//*****************************************************************************
/** @file    Heater.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-NOV-20 Samara created the file
 *  @date 2020-DEC-01 Samara finished the file
 * 
*/

#include "Heater.h"

// External Shares

/** @var float Temp_Reading
 *  Variable that holds the measured temperature
*/
extern Share <float> Temp_Reading;
/**
 *  @var float Temp_Setpoint
 *  Variable that holds the desired temperature
*/
extern Share <float> Temp_Setpoint;


/** 
 *  @brief   Task to control a simple heating pad.
 *  @details This code includes a task that operates a simple heating by
 *           writing to the pin that the pad is attached to either high or 
 *           low. HIGH turns the pad on, while LOW turns it off. This code 
 *           also tests which state the heating pad should be in by comparing
 *           the measured and desired value for temperature.
 * @param   p_params A pointer to function parameters which we don't use.
*/
void Power_Dissipate_Task (void* p_params)
{
    (void)p_params;                     // Shuts up a compiler warning

    // Set pin PB6 (or D10 on Arduino) for OUTPUT
    pinMode(PB6, OUTPUT);

    // Set variables for shares
    float measured_temp;
    float desired_temp;

    // Set up the states for the state transition model
    uint8_t state;
    uint8_t S0_INIT = 0;
    uint8_t S1_OFF = 1;
    uint8_t S2_ON = 2;

    // Set the first state as State 0
    state = S0_INIT;

    // Start the main code
    for(;;)
    {
        if(state == S0_INIT)
        {
            // State 0 Code

            // Turn the heater off by setting the pin to LOW
            digitalWrite(PB6, LOW);

            // Change the state to State 1
            state = S1_OFF;

        }
        else if(state == S1_OFF)
        {
            // State 1 Code

            // Get the data from the shares and assign them to variables
            Temp_Reading.get(measured_temp);
            Temp_Setpoint.get(desired_temp);

            // Test the data to determine whether the heating pad should be turned on
            if (measured_temp < desired_temp)
            {
                digitalWrite(PB10, HIGH);
                state = S2_ON;
            }
            else
            {
                digitalWrite(PB10, LOW);
                state = S1_OFF;
            }


        }
        else if(state == S2_ON);
        {
            // State 2 Code

            // Get the data from the shares and assign it to variables
            Temp_Reading.get(measured_temp);
            Temp_Setpoint.get(desired_temp);

            // Test the data to determine if the heating pad should be turned off
            if (measured_temp < desired_temp)
            {
                digitalWrite(PB10, HIGH);
                state = S2_ON;
            }
            else
            {
                digitalWrite(PB10, LOW);
                state = S1_OFF;
            }
        }

    }

}