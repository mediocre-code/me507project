//*****************************************************************************
/** @file    SolenoidValve.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-NOV-30 Samara finished the file
 *
*/

#include "SafetyCheck.h"

extern Share <float> Percent_Valve_Open;
extern Share <float> Duty_Cycle;
extern Share <float> Heater_Power;
extern Share<float> Mister_Voltage;

/**  @brief   Source code to control a simple Solenoid valve.
 *  @details This code includes a task that operates a simple Solenoid valve by
 *  writing to the pin that the valve is attached to either high or low. HIGH
 *  opens the valve, while LOW closes it.
 *  @param   p_params A pointer to function parameters which we don't use.
*/
void Safety_Check_Task (void* p_params)
{
    (void)p_params;                     // Shuts up a compiler warning

}