//*****************************************************************************
/** @file    HumidTempSense.cpp
 *  @author  Joshua Clemens, Jacob Everest, Samara Van Blaricom
 *
 *  @date 2020-OCT-27 Samara created the file
 *  @date 2020-DEC-01 Samara finished the file
 *
*/
#include "HumidTempSense.h"

Share<float> Rel_Humidity ("Air Humidity");
Share<float> Temp_Reading ("Temperature");

/**  @brief   Task used to operate the Qwiic Humidity Sensor - SHTC3 to measure
 *           temperature and humidity.
 *  @details This task uses the class SHTC3 provided by SparkFun under the 
 *           header SparkFun_SHTC3.h to operate the Qwiic Humidity Sensor. The
 *           code in this task creates an object from the class and calls on
 *           update method to measure the temperature and humidity of an area.
 *           It then sends the data to another task through the use of a share.
 *  @param   p_params A pointer to function parameters which we don't use.
*/
void Temp_Humidity_Sensor_Task (void* p_params)
{
    (void)p_params;                     // Shuts up a compiler warning
    float PercHumid;
    float Temp;

    // Set up the states for the state transition model
    uint8_t state;
    uint8_t S0_INIT = 0;
    uint8_t S1_READ = 1;
    uint8_t S2_SEND = 2;


    SHTC3 mySHTC3 = SHTC3();              // Declare an instance of the SHTC3 class

    // Read the status of the sensor and print the result
    switch(mySHTC3.begin())                
    {
        case SHTC3_Status_Nominal : Serial.print("Nominal\n"); break;
        case SHTC3_Status_Error : Serial.print("Error Humdity Sensor not working\n"); break;
        case SHTC3_Status_CRC_Fail : Serial.print("CRC Fail\n"); break;
        default : Serial.print("Unknown return code\n"); break;
    }

    // Set the state to S0_INIT
    state = S0_INIT;

    for(;;)
    {
        if (state == S0_INIT)
        {
            // State 0 Code

            // Call "update()" to command a measurement, wait for measurement 
            // to complete, and update the RH and T members of the object
            mySHTC3.update();      
            state = S1_READ;    // switch states     

        }

        else if (state == S1_READ)
        {
            // State 1 Code

            PercHumid = mySHTC3.toPercent();        // get humidity as a percentage
            Temp = mySHTC3.toDegF();                // get temperature in Fahrenheit
            state = S2_SEND;                        // switch states

        }

        else if (state == S2_SEND)
        {
            // State 2 Code

            Rel_Humidity.put(PercHumid);            // Set the measured humidity to Rel_Humidity
            Temp_Reading.put(Temp);                 // Set the measured temperature to Temp_Reading

        }

        else
        {
        Serial << "Invalid State" << endl;
        state = S0_INIT;
        
        }
    }


}


          
  