var files_dup =
[
    [ "baseshare.cpp", "baseshare_8cpp.html", "baseshare_8cpp" ],
    [ "baseshare.h", "baseshare_8h.html", "baseshare_8h" ],
    [ "Heater.cpp", "Heater_8cpp.html", "Heater_8cpp" ],
    [ "Heater.h", "Heater_8h.html", "Heater_8h" ],
    [ "HumidTempSense.cpp", "HumidTempSense_8cpp.html", "HumidTempSense_8cpp" ],
    [ "HumidTempSense.h", "HumidTempSense_8h.html", "HumidTempSense_8h" ],
    [ "LightClass.cpp", "LightClass_8cpp.html", null ],
    [ "LightClass.h", "LightClass_8h.html", [
      [ "LightClass", "classLightClass.html", "classLightClass" ]
    ] ],
    [ "Mister.cpp", "Mister_8cpp.html", "Mister_8cpp" ],
    [ "Mister.h", "Mister_8h.html", "Mister_8h" ],
    [ "MoistureSense.cpp", "MoistureSense_8cpp.html", "MoistureSense_8cpp" ],
    [ "MoistureSense.h", "MoistureSense_8h.html", "MoistureSense_8h" ],
    [ "MotorRun.h", "MotorRun_8h_source.html", null ],
    [ "SafetyCheck.h", "SafetyCheck_8h_source.html", null ],
    [ "SolenoidValve.cpp", "SolenoidValve_8cpp.html", "SolenoidValve_8cpp" ],
    [ "SolenoidValve.h", "SolenoidValve_8h.html", "SolenoidValve_8h" ],
    [ "taskqueue.h", "taskqueue_8h.html", [
      [ "Queue", "classQueue.html", "classQueue" ]
    ] ],
    [ "taskshare.h", "taskshare_8h.html", [
      [ "Share", "classShare.html", "classShare" ]
    ] ],
    [ "useLightclass.h", "useLightclass_8h_source.html", null ],
    [ "UserInterface.cpp", "UserInterface_8cpp.html", "UserInterface_8cpp" ],
    [ "UserInterface.h", "UserInterface_8h_source.html", null ]
];