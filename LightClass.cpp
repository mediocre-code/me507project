 /** @file LightClass.cpp
 *         class to save the value from the light sensor.
 *    
 *  @author Jacob Everest, Samara VanBlaricom, Joshua Clemens
 *  @date  2020-Nov-11 Original file
 */

#include <Arduino.h>
#include <PrintStream.h>
// #include <Wire.h>
// #include <BH1750.h>
#include <LightClass.h>
// BH1750 lightMeter(0x23);

/** @brief   Save the value of light from y to lightvalue
 *  @details Save the value of light from y to lightvalue and use it for later work.
 */

LightClass::LightClass (float y)
{
    lightvalue = y;
}

/** @brief   Present lightvalue 
 */

float LightClass::lightlevel (void)
{
    return lightvalue;
}
//lightMeter.readLightLevel()